import time
import urllib
import getpass
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select
from bs4 import BeautifulSoup

class dbs(object):
	__url = 'https://secure.id.dbsdigibank.com/'
	def __init__(self):
		self.formLogin()

	def formLogin(self):
		''' webdriver : Chrome() Firefox() PhantomJS() '''
		username = input('Masukkan username: ')
		password = getpass.getpass('Masukkan password: ')
		# self.__driver = webdriver.PhantomJS()
		# self.__driver.wait = WebDriverWait(self.__driver, 3)
		self.__option = webdriver.ChromeOptions()
		self.__option.add_argument("--incognito")
		self.__option.add_argument("--start-maximized")
		self.__option.add_argument("headless")
		self.__driver = webdriver.Chrome(options=self.__option)
		self.__driver.wait = WebDriverWait(self.__driver, 10)

		self.__username = username
		self.__password = password
		self.authLogin()

	def authLogin(self):
		try:
			self.__driver.get(self.__url)
			username = self.__driver.wait.until(EC.presence_of_element_located((By.XPATH, "//input[@name=\"username\"]")))
			password = self.__driver.wait.until(EC.presence_of_element_located((By.XPATH, "//input[@name=\"password\"]")))
			loginBTN = self.__driver.wait.until(EC.presence_of_element_located((By.XPATH, "//button[@class=\"Button__ButtonType-gzYGsv dotbSK\"]")))

			username.send_keys(self.__username)
			password.send_keys(self.__password)
			loginBTN.send_keys(webdriver.common.keys.Keys.SPACE)

			try:
				self.__driver.wait.until(EC.presence_of_element_located((By.XPATH, "//input[@label=\"OTP\"]")))
				self.otp()
				self.showMenu()
			except:
				alert = self.__driver.switch_to.alert()
				print(alert.text)
				alert.accept()
		except:
			print("please check your connection...")

	def showMenu(self):
		print ("""
	       1. Cek Saldo
	       2. Cek Mutasi Hari ini
	       3. Exit/Quit
	       *************************************************************************
	       (Script ini masih beta, hanya bisa pilih 1 menu kemudian otomatis logout)
	       """)

		menu = input("Pilih Menu : ")
		if (menu =='1') :
			self.cekSaldo()
			self.logout()
		elif (menu == '2'):
			self.cekMutasi()
			self.logout()
		elif (menu == '3'):
			self.logout()

		self.__driver.quit()
		exit()

	def otp(self):
		try :
			inputOtp = input("masukkan OTP: ")

			otpPin = self.__driver.wait.until(EC.presence_of_element_located((By.XPATH,"//input[@name=\"password\"]")))
			loginBTN = self.__driver.wait.until(EC.presence_of_element_located((By.XPATH, "//button[@class=\"Button__ButtonType-gzYGsv dotbSK\"]")))

			otpPin.send_keys(inputOtp)
			loginBTN.send_keys(webdriver.common.keys.Keys.SPACE)
		except TimeoutException:
		    print("Otp failed. Check your code")

	def logout(self):
		try :
			logout = self.__driver.wait.until(EC.presence_of_element_located((By.XPATH,"//a[@href=\"/logout\"]")))
			logout.click()
			print("Anda berhasil logout")
		except TimeoutException:
		    print("Session timeout. please login again")

	def cekMutasi(self):
		# mainMenuMutasi = self.__driver.wait.until(EC.presence_of_element_located((By.XPATH, "//div[contains(@class, \"HamMenuItem__Title-kxnncH itDOVo title\") or starts-with(., \"Rekening saya\")]")))
		mainMenuMutasi = self.__driver.wait.until(EC.presence_of_element_located((By.XPATH, "//div[@class=\"HamMenuItem__Row-cxuXEZ daMLcB\"][2]")))
		mainMenuMutasi.click()

		menuMutasi = self.__driver.wait.until(EC.element_to_be_clickable((By.XPATH, "//a[@href=\"/home/primary\"]")))
		menuMutasi.click()
		try:
			self.__driver.wait.until(EC.presence_of_element_located((By.XPATH, "//div[@class=\"styledHistory__HistoryWrap-cNIhYv etred\"]")))
			mutasi = self.__driver.find_element_by_xpath("//div[contains(@class, \"styledHistory__HistoryWrap-cNIhYv etred\") and starts-with(., \"16 JUL\")]")
			self.__mutasiData = self.mutasiParse(mutasi)
			self.showMutasi()
		except:
			print("Tidak ada mutasi hari ini")

		self.__driver.switch_to.default_content()

	def mutasiParse(self,table):
		table_mutasi = BeautifulSoup(table.get_attribute('outerHTML'),"html.parser")
		# print(table_mutasi.prettify())
		data = []
		table_body = table_mutasi.find('div', {'class': 'styledHistory__HistoryWrap-cNIhYv etred'})
		rows = table_body.find_all('div', {'class': ''})
		for row in rows :
			cols = row.find_all('div', {'class': 'styledHistory__HistoryDisplayWrap-dAekJv jLKsod'})
			for ele in cols:
				if cols:
					cols = []
					cols.append('16 JUL')
					cols.append(ele.find('div', {'class': 'styledHistory__HistoryTwoRight-gJlhPT jyUzFP'}).text.strip())
					cols.append(ele.find('div', {'class': 'styledHistory__HistoryDisplayLineOne-vuwCt iGLQzG'}).text.strip())
					cols.append(ele.find('div', {'class': 'styledHistory__HistoryDescription-wSDdN lofPwe'}).text.strip() if ele.find_all('div', {'class': 'styledHistory__HistoryDescription-wSDdN lofPwe'}) else "-")
			data.append([ele for ele in cols if ele])
		return data[0:]

	def showMutasi(self) :
		if len(self.__mutasiData) > 0 :
			print("Tgl \tMutasi \t\t\t\tDebit/Kredit \t\t\t\tKet")
			for transaction in self.__mutasiData:
				if transaction:
					print("%s \t%s \t\t\t\t%s \t\t\t\t%s"%(transaction[0], transaction[1], transaction[2], transaction[3]))
		else:
			print("Tidak ada Mutasi Hari ini")

	def cekSaldo(self):
		try:
			mutasi = self.__driver.wait.until(EC.presence_of_element_located((By.XPATH, "//span[@class=\"DashboardCard__CardAmount-jXPVTt gOBZaq\"]")))
			saldo = mutasi.text

			print("Saldo DBS saat ini adalah %s"%saldo)
			self.__driver.switch_to.default_content()
		except TimeoutException:
		    print("Session timeout. please login again")


if __name__ == "__main__":
	dbs()
